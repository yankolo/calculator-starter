package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;
    double num1;
    double num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        if (getNums()) {
            result.setText(Double.toString(num1 + num2));
        }
    }  //addNums()

    public void substractNums(View v) {
        if (getNums()) {
            result.setText(Double.toString(num1 - num2));
        }
    }

    public void multiplyNums(View v) {
        if (getNums()) {
            result.setText(Double.toString(num1 - num2));
        }
    }

    public void devideNums(View v) {
        if (getNums()) {
            if (num2 == 0) {
                result.setText(R.string.cant_divide_by_0);
            } else {
                result.setText(Double.toString(num1 - num2));
            }
        }
    }

    public void clear(View v) {
        etNumber1.setText("");
        etNumber2.setText("");
        num1 = 0;
        num2 = 0;
    }

    private boolean getNums() {
        if (etNumber1.getText().toString().isEmpty() || etNumber2.getText().toString().isEmpty()) {
            result.setText(R.string.empty_fields);
            return false;
        } else {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            return true;
        }
    }

}